"""
    Master 2 Health Data Science 2022-2024
    Sujet de NLP - Naima Oubenali
    
    Groupe 8 : Traduction automatique d’abstracts PubMed et extraction des mots clés
    Utiliser des modèles de traduction automatique pour traduire des abstracts PubMed dans différentes langues (3 minimum) 
    et l’extraction/ génération des mots clés qui décriraient le mieux le contenu des abstracts.

    
    Groupe composé de : 
    - Tracy LURANT
    - Antoine TESTON
    - Malik MAMMAR
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    NLP - BERT
    Extraction des mots clés
    
    Concentration sur les domaines Données synthetic, Digital Twins, Mirors cohorts
"""
import string
import streamlit as st
import requests
from bs4 import BeautifulSoup
from keybert import KeyBERT
from transformers import MarianMTModel, MarianTokenizer, pipeline
import deepl
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.translate.bleu_score import sentence_bleu
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import streamviz


DEEPL_API_KEY = "" # A supp avant le rendu

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')


def search_pubmed(query, max_results=10):
    """
    Recherche des articles sur PubMed en fonction d'une requête donnée.

    Args:
        query (str): La requête de recherche à utiliser pour trouver des articles sur PubMed.
        max_results (int): Le nombre maximum de résultats à retourner (par défaut 10).

    Returns:
        list: Une liste d'identifiants d'articles PubMed correspondant à la requête.
    """
    url = f"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={query}&retmax={max_results}&retmode=json"
    response = requests.get(url)
    results = response.json()
    id_list = results.get('esearchresult', {}).get('idlist', [])
    return id_list

def get_pubmed_article_details(pubmed_id):
    """
    Obtient les détails d'un article PubMed en utilisant son identifiant.

    Args:
        pubmed_id (str): L'identifiant de l'article PubMed.

    Returns:
        tuple: Un tuple contenant le titre, le résumé et les mots clés de l'article.
    """
    url = f"https://pubmed.ncbi.nlm.nih.gov/{pubmed_id}/"
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')

    title = soup.find('h1', {'class': 'heading-title'})
    if title:
        title = title.get_text(strip=True)
    else:
        title = "Titre non trouvé"

    abstract_div = soup.find('div', {'class': 'abstract-content'})
    if abstract_div:
        abstract = abstract_div.get_text(strip=True)
    else:
        abstract = "Abstract non trouvé"

    return title, abstract

def extract_keywords_keybert(text, num_keywords=5):
    """
    Extrait les mots clés d'un texte donné en utilisant KeyBERT.

    Args:
        text (str): Le texte à analyser pour extraire des mots clés.
        num_keywords (int): Le nombre de mots clés à extraire (par défaut 5).

    Returns:
        str: Une chaîne de caractères des mots clés extraits, séparés par des virgules.
    """
    kw_model = KeyBERT(model='all-MiniLM-L6-v2')
    keywords = kw_model.extract_keywords(text, keyphrase_ngram_range=(1, 2), stop_words=None, top_n=num_keywords)
    return ', '.join([kw[0] for kw in keywords])

def preprocess_abstract_tfidf(text):
    """
    Prétraite un texte donné pour l'analyse TF-IDF.

    Args:
        text (str): Le texte à prétraiter.

    Returns:
        str: Une chaîne de caractères des mots lemmatisés, séparés par des virgules.
    """
    # Suppression des espaces inutiles
    review_text = text.strip()
    # Conversion en minuscules
    review_text = review_text.lower()
    # Suppression de la ponctuation
    review_text = review_text.translate(str.maketrans('', '', string.punctuation))
    # Tokenisation
    tokens = word_tokenize(review_text)
    # Suppression des stopwords
    stop_words = set(stopwords.words('english'))
    tokens = [word for word in tokens if word not in stop_words]
    # Stemming
    porter = PorterStemmer()
    stemmed_tokens = [porter.stem(word) for word in tokens]
    # Lemmatisation
    lemmatizer = WordNetLemmatizer()
    lemmatized_words = [lemmatizer.lemmatize(word) for word in stemmed_tokens]
    # Retourne le texte prétraité
    return ', '.join(lemmatized_words)

def extract_keywords_tfidf(text, num_keywords=5):
    """
    Extrait les mots clés d'un texte donné en utilisant TF-IDF.

    Args:
        text (str): Le texte à analyser pour extraire des mots clés.
        num_keywords (int): Le nombre de mots clés à extraire (par défaut 5).

    Returns:
        list: Une liste des mots clés extraits du texte.
    """
    cleaned_text = preprocess_abstract_tfidf(text)
    vectorizer = TfidfVectorizer(ngram_range=(1, 2))
    tfidf_matrix = vectorizer.fit_transform([cleaned_text])
    feature_names = vectorizer.get_feature_names_out()
    tfidf_scores = tfidf_matrix.toarray()[0]
    sorted_indices = tfidf_scores.argsort()[::-1]
    top_keywords = feature_names[sorted_indices][:num_keywords]
    return ', '.join(top_keywords)

def split_text(text):
    """
    Sépare le texte en phrases, chaque phrase formant un chunk.

    Args:
        text (str): Le texte à séparer.

    Returns:
        list: Une liste de chunks de texte, où chaque chunk est une phrase complète.
    """
    # Tokenisation en phrases
    sentences = nltk.sent_tokenize(text)
    
    return sentences

def translate_with_deepl(text, tgt_lang):
    translator = deepl.Translator(DEEPL_API_KEY)
    result = translator.translate_text(text, target_lang=tgt_lang)
    return result.text

def translate_text(text, src_lang, tgt_lang, model_choice):
    """
    Translate text using the specified translation model.
    
    Args:
        text (str): Text to translate.
        src_lang (str): Source language code.
        tgt_lang (str): Target language code.
        model_choice (str): Translation model choice ('MarianMT' or 'GoogleT5').
    
    Returns:
        str: Translated text.
    """
    
    if model_choice == 'MarianMT':
        model_name = f'Helsinki-NLP/opus-mt-{src_lang}-{tgt_lang}'
        tokenizer = MarianTokenizer.from_pretrained(model_name)
        model = MarianMTModel.from_pretrained(model_name)
        chunks = split_text(text)
        translated_chunks = []
        for chunk in chunks:
            translated = model.generate(**tokenizer(chunk, return_tensors="pt", padding=True))
            tgt_text = [tokenizer.decode(t, skip_special_tokens=True) for t in translated]
            translated_chunks.append(tgt_text[0])
        return ' '.join(translated_chunks)
    
    elif model_choice == 'GoogleT5':
        translation_pipeline = pipeline(f'translation_{src_lang}_to_{tgt_lang}', model='t5-base', tokenizer='t5-base')
        chunks = split_text(text)
        translated_chunks = []
        for chunk in chunks:
            translated = translation_pipeline(chunk, max_length=500, truncation=True)
            translated_chunks.append(translated[0]['translation_text'])
        return ' '.join(translated_chunks)
    
    else:
        return "Modèle de traduction non supporté"

def calculate_bleu(reference_text, tgt_lang, candidate_text):
    """
    Calculate the BLEU score for a translated text.
    
    Args:
        reference_text (str): Reference text.
        tgt_lang (str): Target language code.
        candidate_text (str): Candidate translated text.
    
    Returns:
        float: BLEU score.
    """
    
    reference = [translate_with_deepl(reference_text, tgt_lang).split()]
    candidate = candidate_text.split()
    score = sentence_bleu(reference, candidate)
    plot_bleu_gauge(score)
    return score

def plot_bleu_gauge(score):
    """
    Plot a gauge chart to display the BLEU score.
    
    Args:
        score (float): BLEU score.
    """
    # Définir les critères de score BLEU
    categories = [
        "Presque inutile",
        "Difficile de saisir l'essentiel",
        "L'essentiel est clair, mais comporte des erreurs grammaticales significatives.",
        "Compréhensible pour de bonnes traductions",
        "Des traductions de haute qualité",
        "Des traductions de très haute qualité, adéquates et fluides",
        "Une qualité souvent supérieure à celle des humains"
    ]
    thresholds = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0]

    # Déterminer la catégorie correspondant au score
    category_index = next(i for i, threshold in enumerate(thresholds) if score < threshold) - 1
    category_label = categories[category_index]

    __YOUR_TITLE__ = f"Score BLEU: {category_label}"
    
    # Créer un diagramme en jauge pour afficher le score BLEU
    streamviz.gauge(
    gVal=score, gSize="MED", gTheme="white",
    gTitle=__YOUR_TITLE__, gMode="gauge+number",
    grLow=.3, grMid=.5)