"""
    Master 2 Health Data Science 2022-2024
    Sujet de NLP - Naima Oubenali
    
    Groupe 8 : Traduction automatique d’abstracts PubMed et extraction des mots clés
    Utiliser des modèles de traduction automatique pour traduire des abstracts PubMed dans différentes langues (3 minimum) 
    et l’extraction/ génération des mots clés qui décriraient le mieux le contenu des abstracts.

    
    Groupe composé de : 
    - Tracy LURANT
    - Antoine TESTON
    - Malik MAMMAR
    
    TO DO : 
    REQUIREMENT.TXT
    README.MD
    DOCSTRING des fonctions

    Fonctionnalités :
    Webscraping
    NLP - BERT
    Extraction des mots clés
    
    Concentration sur les domaines Données synthetic, Digital Twins, Mirors cohorts
"""

# Importation des libraries
import streamlit as st


from extract import search_pubmed, get_pubmed_article_details, extract_keywords_keybert, extract_keywords_tfidf, translate_text, calculate_bleu, plot_bleu_gauge


# Dictionnaire pour mapper les noms des langues aux codes de langue
language_dict = {
    "Français": "fr",
    "Roumain": "ro",
    "Allemand": "de"
}

# Initialiser les variables de session si elles n'existent pas
if 'translated_abstract_marianmt' not in st.session_state:
    st.session_state.translated_abstract_marianmt = ""
if 'translated_abstract_googlet5' not in st.session_state:
    st.session_state.translated_abstract_googlet5 = ""
if 'bleu_score_marianmt' not in st.session_state:
    st.session_state.bleu_score_marianmt = None
if 'bleu_score_googlet5' not in st.session_state:
    st.session_state.bleu_score_googlet5 = None


# Interface utilisateur de Streamlit
st.sidebar.title("Recherche PubMed")
query = st.sidebar.text_input("Entrez votre recherche PubMed, PMID ou DOI:", "machine learning cancer")
max_results = st.sidebar.number_input("Nombre maximal de résultats:", min_value=1, max_value=100, value=10)

# Variable pour le retour en arrière
if 'back' not in st.session_state:
    st.session_state.back = False

# Gestion de l'état de retour en arrière
def go_back():
    st.session_state.back = True

if st.sidebar.button("Rechercher"):
    st.session_state.back = False
    with st.spinner('Recherche en cours...'):
        pubmed_ids = search_pubmed(query, max_results)
        articles = [get_pubmed_article_details(pubmed_id) for pubmed_id in pubmed_ids]

        if articles:
            st.session_state.articles = articles
            st.session_state.pubmed_ids = pubmed_ids
        else:
            st.session_state.articles = []
            st.session_state.pubmed_ids = []

# Affichage des résultats
if "articles" in st.session_state and not st.session_state.back:
    articles = st.session_state.articles
    pubmed_ids = st.session_state.pubmed_ids

    if articles:
        st.sidebar.success(f"Trouvé {len(articles)} articles.")
        for idx, (title, abstract) in enumerate(articles):
            if st.sidebar.button(title, key=pubmed_ids[idx]):
                st.session_state["selected_pubmed_id"] = pubmed_ids[idx]
    else:
        st.sidebar.error("Aucun article trouvé.")

# Affichage de l'article sélectionné
if "selected_pubmed_id" in st.session_state and not st.session_state.back:
    pubmed_id = st.session_state["selected_pubmed_id"]
    title, abstract = get_pubmed_article_details(pubmed_id)
    
    st.header(title)
    st.subheader("Abstract")
    st.write(abstract)

    st.subheader("Mots Clés Générés")
    keybert_keywords = extract_keywords_keybert(abstract, 5)
    tfidf_keywords = extract_keywords_tfidf(abstract, 5)
    
    tab1, tab2 = st.tabs(["KeyBERT", "TF-IDF"])
    
    with tab1:
        st.write("Mots clés extraits avec KeyBERT :")
        st.write(keybert_keywords)
    
    with tab2:
        st.write("Mots clés extraits avec TF-IDF :")
        st.write(tfidf_keywords)
        
    st.subheader("Traduction")

    tgt_lang_display = st.selectbox("Choisir la langue de traduction", list(language_dict.keys()))
    tgt_lang = language_dict[tgt_lang_display]
    
    if st.button("Traduire"):
        with st.spinner("Traduction en cours..."):
            st.session_state.translated_abstract_marianmt = translate_text(abstract, 'en', tgt_lang, "MarianMT")
            st.session_state.translated_abstract_googlet5 = translate_text(abstract, 'en', tgt_lang, "GoogleT5")
           
    if st.session_state.translated_abstract_marianmt and st.session_state.translated_abstract_googlet5:
        tab1, tab2 = st.tabs(["MarianMT", "GoogleT5"])
        
        with tab1:
            st.markdown("### Traduction avec MarianMT")
            st.write(st.session_state.translated_abstract_marianmt)
            
            st.markdown("---")
            
            if st.button("Scoring de Traductions (MarianMT)", key="MarianMT"):
                bleu_score_marianmt = calculate_bleu(abstract, tgt_lang, st.session_state.translated_abstract_marianmt)
                st.session_state.bleu_score_marianmt = bleu_score_marianmt
        
        with tab2:
            st.markdown("### Traduction avec GoogleT5")
            st.write(st.session_state.translated_abstract_googlet5)
            
            st.markdown("---")
            
            if st.button("Scoring de Traductions (GoogleT5)", key="GoogleT5"):
                bleu_score_googlet5 = calculate_bleu(abstract, tgt_lang, st.session_state.translated_abstract_googlet5)
                st.session_state.bleu_score_googlet5 = bleu_score_googlet5

        if st.session_state.bleu_score_marianmt is not None:
            with tab1:
                st.write(f"Score BLEU: {st.session_state.bleu_score_marianmt}")
        
        if st.session_state.bleu_score_googlet5 is not None:
            with tab2:
                st.write(f"Score BLEU: {st.session_state.bleu_score_googlet5}")

# Pour exécuter streamlit run script_name.py
# 