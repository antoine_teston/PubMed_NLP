# Application de recherche de référence PubMed, génération de mots clés et traduction/évaluation multilangues

## Vue d'ensemble 🔎
Cette application est conçue pour rechercher des articles scientifiques sur PubMed, extraire des mots-clés et traduire des résumés en utilisant différents modèles. Elle offre une interface conviviale pour récupérer des articles PubMed, générer des mots-clés et traduire des textes avec une évaluation de la qualité de la traduction par le score BLEU.

## Fonctionnalités 🪛
- **Recherche PubMed** : Rechercher des articles en utilisant des requêtes PubMed, des PMIDs ou des DOIs.
- **Détails de l'article** : Afficher le titre et le résumé des articles sélectionnés.
- **Extraction de mots-clés** : Extraire des mots-clés en utilisant les méthodes KeyBERT et TF-IDF.
- **Traduction** : Traduire des résumés en plusieurs langues en utilisant les modèles MarianMT et GoogleT5.
- **Qualité de la traduction** : Calculer et afficher les scores BLEU pour les résumés traduits.

## Capture d'écran 

![](../png/01.png)

![](../png/02.png)

![](../png/03.png)

## Instructions d'installation 

### Prérequis
- Python 3.7 ou supérieur
- Bibliothèques Python requises : `streamlit`, `requests`, `beautifulsoup4`, `keybert`, `scikit-learn`, `nltk`, `transformers`, `sentencepiece`, `deepl`

### Installation 💿
1. Clonez le dépôt :
    ```bash
    git clone https://gitlab.com/votreutilisateur/PubMed_NLP.git
    cd PubMed_NLP
    ```
2. Installez les paquets requis :
    ```bash
    pip install -r requirements.txt
    ```

### Exécution de l'application
1. Exécutez l'application Streamlit :
    ```bash
    streamlit run app.py
    ```
2. Ouvrez votre navigateur web et accédez à l'URL fournie par Streamlit (généralement `http://localhost:8501`).

## Utilisation

### Recherche d'articles 📝
1. Entrez votre requête PubMed, PMID ou DOI dans la boîte de saisie de la barre latérale.
2. Définissez le nombre maximum de résultats que vous souhaitez récupérer.
3. Cliquez sur le bouton "Rechercher" pour lancer la recherche.
4. L'application affichera une liste de titres d'articles dans la barre latérale.

### Affichage des détails de l'article
1. Cliquez sur un titre d'article dans la barre latérale pour voir ses détails.
2. Le titre et le résumé de l'article sélectionné seront affichés dans le panneau principal.

### Extraction de mots-clés 🔑
1. L'application extrait automatiquement les mots-clés du résumé en utilisant les méthodes KeyBERT et TF-IDF.

> KeyBERT est un modèle basé sur les représentations de phrases produites par le modèle BERT (Bidirectional Encoder Representations from Transformers). Contrairement aux méthodes traditionnelles d'extraction de mots-clés, KeyBERT utilise l'encodage contextuel de BERT pour identifier les mots et phrases les plus pertinents dans un texte.

> KeyBERT commence par encoder l'ensemble du document et chaque mot ou phrase candidat à l'aide de BERT. \n Ensuite, il calcule la similarité entre la représentation du document et celle de chaque candidat à l'aide d'une mesure comme la similarité cosinus. Les mots ou phrases avec la similarité la plus élevée sont considérés comme les mots-clés les plus représentatifs du document et sont donc extraits.
> 
> Son grand avantage est dans sa capacité à générer des phrases de mots-clés multi-mots, et pas seulement des mots simples.

> TF-IDF (Term Frequency-Inverse Document Frequency) est une méthode statistique utilisée pour évaluer l'importance d'un mot dans un document par rapport à un corpus de documents. Il est couramment utilisé dans l'extraction de mots-clés et la recherche d'information. Il mesure la fréquence d'apparition d'un terme dans un document. Plus un terme apparaît souvent dans un document, plus sa valeur TF est élevée. Il a nécessité toutefois une étape importante de pre-processing avec traitement du texte, stemming et lemmatization 

2. Visualisez les mots-clés extraits dans la section "Mots Clés Générés".

### Traduction des résumés
1. Sélectionnez la langue cible dans le menu déroulant de la section "Traduction".
2. Cliquez sur le bouton "Traduire" pour traduire le résumé.
3. Visualisez les traductions sous les onglets respectifs des modèles MarianMT et GoogleT5.

> MarianMT est un modèle de traduction automatique développé par l'équipe Helsinki-NLP. MarianMT est basé sur l'architecture **Transformer** et fait partie de la bibliothèque **Hugging Face**. Il est spécialement conçu pour être rapide et efficace dans la traduction entre plusieurs paires de langues. Il utilise des modèles basés sur les Transformers, qui sont des réseaux de neurones spécialisés pour traiter des séquences de données.
Le modèle est formé sur un vaste corpus de données multilingues, permettant de capturer les nuances linguistiques et contextuelles nécessaires pour des traductions précises. Ses principaux avantages sont :
> - L'Open Source
> - Les performances : solides en termes de précision de traduction
> - Multilingues
>
> GoogleT5 (Text-to-Text Transfer Transformer) est quant à lui un modèle développé par Google Research qui utilise l'architecture Transformer pour traiter diverses tâches de traitement de NLP dans un format textuel unifié. T5 est entraîné à traiter chaque tâche NLP comme une tâche de transformation de texte, y compris la traduction.
>T5 utilise une approche "text-to-text", où toute entrée et sortie de tâche est traitée comme du texte.
Le modèle est pré-entraîné sur un large ensemble de données multilingues, puis affiné pour des tâches spécifiques comme la traduction.
> Le modèle T5 actuellement utilisé en OpenSource est un modèle de base "T5-base" qui mets à disposition uniquement quatre langues : le Français, le Roumain, l'Anglais et l'Allemand, raison pour laquelle ces langues sont uniquement disponible dans la web-app


### Évaluation de la traduction
1. Cliquez sur le bouton "Scoring de Traductions" sous l'onglet de la traduction respective pour calculer le score BLEU.
2. Le score BLEU sera affiché dans l'onglet correspondant.

> Le score BLEU (Bilingual Evaluation Understudy) est une métrique utilisée pour évaluer la qualité des traductions automatiques en comparant la traduction générée par la machine avec une ou plusieurs traductions de référence humaines. Il mesure la précision et la fluidité de la traduction en calculant la correspondance entre les segments de mots (n-grams) de la traduction candidate et ceux des références, tout en pénalisant les traductions trop courtes. Un score BLEU plus élevé indique une meilleure qualité de traduction.

> Le score est effectué par comparaison avec la traduction fournit par DeePL. Vous pouvez insérer votre clé API d'accès au service via cette ligne de commande :

```
    extract.py
    DEEPL_API_KEY = "" # Insérer ici
```

## Dépendances
- `streamlit` : Cadre d'application web pour les applications interactives.
- `requests` : Bibliothèque pour effectuer des requêtes HTTP.
- `beautifulsoup4` : Bibliothèque pour le web scraping.
- `keybert` : Bibliothèque pour l'extraction de mots-clés utilisant les embeddings BERT.
- `scikit-learn` : Bibliothèque de machine learning pour l'extraction de mots-clés.
- `nltk` : Boîte à outils de traitement du langage naturel pour le prétraitement des textes.
- `transformers` : Bibliothèque Hugging Face pour les modèles NLP de pointe.
- `sentencepiece` : Bibliothèque pour la tokenisation des textes.
- `deepl` : Bibliothèque pour la traduction DeepL (facultatif, si vous utilisez DeepL pour la traduction de référence).
- `streamviz` : Bibliothèque de dataviz compagnon de streamlit pour la construction de gauge

## Références 📝

Raffel, C., Shazeer, N., Roberts, A., Lee, K., Narang, S., Matena, M., ... & Liu, P. J. (2020). Exploring the limits of transfer learning with a unified text-to-text transformer. J. Mach. Learn. Res., 21(140), 1-67.

Östling R, Scherrer Y, Tiedemann J, Tang G, Nieminen T. The Helsinki Neural Machine Translation System. In: Proceedings of the Second Conference on Machine Translation [Internet]. Copenhagen, Denmark: Association for Computational Linguistics; 2017 [cité 31 mai 2024]. p. 338‑47. Disponible sur: http://aclweb.org/anthology/W17-4733

Sheng E, Chang KW, Natarajan P, Peng N. Societal Biases in Language Generation: Progress and Challenges. In: Zong C, Xia F, Li W, Navigli R, éditeurs. Proceedings of the 59th Annual Meeting of the Association for Computational Linguistics and the 11th International Joint Conference on Natural Language Processing (Volume 1: Long Papers) [Internet]. Online: Association for Computational Linguistics; 2021 [cité 31 mai 2024]. p. 4275‑93. Disponible sur: https://aclanthology.org/2021.acl-long.330

Derek. REPNOT/streamviz [Internet]. 2024 [cité 31 mai 2024]. Disponible sur: https://github.com/REPNOT/streamviz


